#Deriving the latest base image
FROM python:latest


#Labels as key value pair
LABEL Maintainer="Ali Hussaini"


# Any working directory can be chosen as per choice like '/' or '/home' etc
# i have chosen /usr/app/src
WORKDIR /root

#to COPY the remote file at working directory in container
COPY test.py ./
# Now the structure looks like this '/usr/app/src/test.py'
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
RUN ls
RUN pwd
#CMD instruction should be used to run the software
#contained by your image, along with any arguments.

CMD [ "python", "/root/test.py"]
